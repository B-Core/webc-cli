package main

import (
	"os"
	"webc/commands"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "WebComponent Creator CLI"
	Commander := commands.Create(app)
	app.Usage = "Creating and managing Web Components and Web Components Apps"
	app.Commands = Commander.Commands
	app.Run(os.Args)
}

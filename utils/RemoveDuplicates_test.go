package utils

import (
	"testing"
	"reflect"
)

func TestRemoveDuplicates(t *testing.T) {
	cases := []struct {
		in, want []string
	}{
		{ []string{"test", "test", "test2", "test4", "test2"}, []string{"test", "test2", "test4"} },
		{ []string{"test", "test", "test", "test", "test"}, []string{"test"} },
		{ []string{"test"}, []string{"test"} },
		{ []string{}, []string{} },
		{ []string{"test", "test1"}, []string{"test", "test1"} },
	}
	

	for _, c := range cases {
		result := RemoveDuplicates(c.in)
		if !reflect.DeepEqual(result, c.want) {
			t.Errorf("RemoveDuplicates(%q) == %q, expect %q", c.in, result, c.want)
		}
	}
}
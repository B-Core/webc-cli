package utils

import (
	"testing"
)

func TestReplaceMacro(t *testing.T) {
	cases := []struct {
		in, want string
	}{
		{"{{test}}", "--replaced--"},
		{"test string very long with }{{test}}{parasite", "test string very long with }--replaced--{parasite"},
		{"", ""},
	}
	
	for _, c := range cases {
		result := ReplaceMacro(c.in, "{{test}}", "--replaced--")
		if result != c.want {
			t.Errorf("Reverse(%q) == %q, expect %q", c.in, result, c.want)
		}
	}
}
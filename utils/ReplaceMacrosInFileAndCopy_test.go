package utils

import (
	"testing"
	"os"
	"io/ioutil"
	"path/filepath"
	s "webc/structs"
)

var doneFixturesPath, _ = filepath.Abs("fixtures/done")

var cases = []struct{
	in, out, want string
} {
	{"fixtures/test1.json", "fixtures/done/ComponentName/ComponentName.json", "fixtures/test1.expect.json"},
	{"fixtures/test2.txt","fixtures/done/ComponentName/ComponentName.txt", "fixtures/test2.expect.txt"},
	{"fixtures/test3.html","fixtures/done/ComponentName/index.html", "fixtures/test3.expect.html"},
}

var c = s.Component{
	Name: "ComponentName",
	Type: "WebComponent",
}
var p = s.Project{
	Name: "Test Project",
}

func TestReplaceMacrosInFileAndCopy_createDir(t *testing.T) {
	os.Mkdir(doneFixturesPath, os.ModeDir)
	for _, f := range cases {
		aPath, _ := filepath.Abs(f.in)
		aSrcPath, _ := filepath.Abs("fixtures")
		aDstPath, _ := filepath.Abs("fixtures/done")
		file, _ := os.Open(aPath)
		defer file.Close()

		ReplaceMacrosInFileAndCopy(file, aSrcPath,aDstPath, c, p, true)

		outPath, _ := filepath.Abs(f.out)
		wantPath, _ := filepath.Abs(f.want)
		f1, _ := ioutil.ReadFile(outPath)
		f2, _ := ioutil.ReadFile(wantPath)
		
		if string(f1) != string(f2) {
			t.Errorf("ReplaceMacrosInFileAndCopy(%q, %s, %s, %q, %q, %t) == %q, expect %q", 
				f, aSrcPath,aDstPath, c, p, true,
				f1,
				f2,
			)
		}
	}
	os.RemoveAll(doneFixturesPath)
}

var cases2 = []struct{
	in, out, want string
} {
	{"fixtures/test1.json", "fixtures/done/ComponentName.json", "fixtures/test1.expect.json"},
	{"fixtures/test2.txt","fixtures/done/ComponentName.txt", "fixtures/test2.expect.txt"},
	{"fixtures/test3.html","fixtures/done/index.html", "fixtures/test3.expect.html"},
}
func TestReplaceMacrosInFileAndCopy(t *testing.T) {
	os.Mkdir(doneFixturesPath, os.ModeDir)
	for _, f := range cases2 {
		aPath, _ := filepath.Abs(f.in)
		aSrcPath, _ := filepath.Abs("fixtures")
		aDstPath, _ := filepath.Abs("fixtures/done")
		file, _ := os.Open(aPath)
		defer file.Close()

		ReplaceMacrosInFileAndCopy(file, aSrcPath,aDstPath, c, p, false)

		outPath, _ := filepath.Abs(f.out)
		wantPath, _ := filepath.Abs(f.want)
		f1, _ := ioutil.ReadFile(outPath)
		f2, _ := ioutil.ReadFile(wantPath)
		
		if string(f1) != string(f2) {
			t.Errorf("ReplaceMacrosInFileAndCopy(%q, %s, %s, %q, %q, %t) == %q, expect %q", 
				f, aSrcPath,aDstPath, c, p, true,
				f1,
				f2,
			)
		}
	}
	os.RemoveAll(doneFixturesPath)
}
package utils

import (
	"os"
	"regexp"
	"sorter"
	"sort"
	"path/filepath"
	"github.com/pinzolo/casee"
	s "webc/structs"
)

var macroRegExp, err = regexp.Compile("{{[A-Z_-]+}}")

// Exists check if a given file exists
// return a boolean true if exists false if not
func Exists(filename string) bool {
	_, exists := os.Open(filename)
	if exists == nil {
		return true
	}
	return false
}

// RemoveDuplicates remove duplicates from an array
// returning an unordered array free of duplicates
func RemoveDuplicates(elements []string) []string {
	encountered := map[string]bool{}

	// Create a map of all unique elements.
	for v := range elements {
		encountered[elements[v]] = true
	}

	// Place all keys from the map into a slice.
	result := []string{}
	for key := range encountered {
		result = append(result, key)
	}

	sort.Strings(result)
	sort.Sort(sorter.ByCase(result))

	return result
}

// ReplaceMacro replace a given macro in a string
// and return the modified string
func ReplaceMacro(data string, macro string, replacement string) string {
	re := regexp.MustCompile(macro)
	return re.ReplaceAllString(data, replacement)
}

// ReplaceMacrosInFileAndCopy replace all macros in a file
// and copy the file on a destination on the disk
func ReplaceMacrosInFileAndCopy(file *os.File, filepsrc string, filepdst string, c s.Component, p s.Project, createDir bool) string {
	fileStat, _ := file.Stat()
	reExt := regexp.MustCompile("\\..+")
	fileData := make([]byte, fileStat.Size())
	// read the project file
	_, err = file.Read(fileData)
	if err != nil {
		panic(err)
	}

	macros := RemoveDuplicates(macroRegExp.FindAllString(string(fileData), -1))
	newData := string(fileData)
	for _, macro := range macros {
		if macro == "{{P_NAME}}" {
			newData = ReplaceMacro(newData, "{{P_NAME}}", p.Name)
		} else if macro == "{{C_NAME}}" {
			newData = ReplaceMacro(newData, "{{C_NAME}}", casee.ToChainCase(c.Name))
		} else if macro == "{{NAME}}" {
			newData = ReplaceMacro(newData, "{{NAME}}", c.Name)
		} else if macro == "{{TYPE}}" {
			newData = ReplaceMacro(newData, "{{TYPE}}", c.Type)
		}
	}

	var nFile *os.File;
	ext := reExt.FindString(file.Name())
	var lastPartFileName string

	// if html
	// replace name by index to allow import without 
	// componentName duplication
	if ext != ".html" {
		lastPartFileName = c.Name + ext
	} else {
		lastPartFileName = "index" + ext
	}

	if createDir {
		folderName := filepath.Join(filepdst, c.Name)
		fileName := filepath.Join(
			folderName,
			lastPartFileName,
		)
		nFile, err = os.Create(fileName)
		if err != nil {
			err = os.Mkdir(folderName, os.ModeDir)
			if err != nil {
				panic(err)
			}
			nFile, err = os.Create(fileName)
		}
	} else {
		folderName := filepath.Join(filepdst)
		fileName := filepath.Join(
			folderName,
			lastPartFileName,
		)
		nFile, err = os.Create(fileName)
		if err != nil {
			panic(err)
		}
	}
	defer nFile.Close()

	nFile.WriteString(newData)
	return newData
}
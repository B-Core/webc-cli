#! /bin/bash

for directory in ./*/
do
  if [ -d $directory ] && [[ $directory != *"templates"* ]] && [[ $directory != *"tests"* ]] && [[ $directory != *"config"* ]]
  then  
    go test "${directory/\./'webc'}"
  fi
done
const mapValue = (value, low1, high1, low2, high2) => low2 + (value - low1) * (high2 - low2) / (high1 - low1);

class WebCHeader extends WebComponent {
  constructor() {
    super("WebCHeader", 
    {
      data: {
        'title': null,
        'message': null,
        'count': 0,
      },
      style: {
        'background-color': null,
        'margin-top': null,
      },
    });
  }

  changeColor(event) {
    const val = mapValue(event.clientX, 0, window.innerWidth, 0, 255);
    const color = `rgb(${parseInt(val)}, ${parseInt(val)}, ${parseInt(val)})`;
    this.set('style', 'background-color', color);
  }

  incrementCounter() {
    this.set('data', 'count', parseInt(this.observables.data.count)+1);
    this.set('data', 'message', this.observables.data.message);
  }

  created() {}

  static get observedAttributes() {
    return [
      'data-title',
      'data-message',
      'data-count',
      'style-background-color',
      'style-margin-top',
    ];
  }
}

window.customElements.define("webc-header", WebCHeader);
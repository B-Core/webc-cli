package workers

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	gs "github.com/termie/go-shutil"
)

// GetCurrentExecDir gets the current program directory
func GetCurrentExecDir() (dir string, err error) {
	path, err := exec.LookPath(os.Args[0])
	if err != nil {
			fmt.Printf("exec.LookPath(%s), err: %s\n", os.Args[0], err)
			return "", err
	}

	absPath, err := filepath.Abs(path)
	if err != nil {
			fmt.Printf("filepath.Abs(%s), err: %s\n", path, err)
			return "", err
	}

	dir = filepath.Dir(absPath)

	return dir, nil
}

var processFolder, _ = GetCurrentExecDir()
// ProcessFolder = process folder (where the code lies) 
var ProcessFolder = filepath.Join(processFolder, "..")
// FileSeparator = OS FileSeparator to string
var FileSeparator = string(os.PathSeparator)

// WorkerCopyTree copy a tree recursively
// from src to dst
func WorkerCopyTree(done chan bool, src string, dst string) {
	err := gs.CopyTree(src, dst, nil)
	if err != nil {
		fmt.Printf("Worker CopyTree Error -> %s\n\n", err)
		done <- false
	}
	done <- true
}

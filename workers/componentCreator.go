package workers

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	s "webc/structs"
	u "webc/utils"
	config "webc/config"
)

func checkC(done chan s.Component, c s.Component, err error) {
	if err != nil {
		fmt.Printf("Worker componentCreator Error -> %s\n\n", err)
		done <- c
	}
}

func treatFile(filepsrc string, filepdst string, done chan s.Component, component *s.Component, p s.Project) {
	f, err := os.Open(filepsrc)
	checkC(done, *component, err)
	defer f.Close()
	checkC(done, *component, err)
	u.ReplaceMacrosInFileAndCopy(f, filepsrc, p.ComponentsDirectory, *component, p, true)
}

// CreateComponent is a worker that will take component and project information
// and will create the files associated to this component in the given Project directory
func CreateComponent(done chan s.Component, name string, Type string, p s.Project) {
	component := *s.CreateComponent(name, Type)
	templateDirName := filepath.Join(ProcessFolder, config.TemplateDirectory, component.Type)
	templateDir, err := ioutil.ReadDir(templateDirName)
	if err != nil {
		templateDirName = filepath.Join(p.TemplateDirectory, component.Type)
		templateDir, err = ioutil.ReadDir(templateDirName)
	}
	checkC(done, component, err)

	for _, file := range templateDir {
		filepsrc, _ := filepath.Abs(filepath.Join(templateDirName, file.Name()))
		filepdst, _ := filepath.Abs(filepath.Join(p.ComponentsDirectory, name, file.Name()))
		treatFile(filepsrc, filepdst, done, &component, p)
	}

	done <- component
}

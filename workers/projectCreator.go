package workers

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	config "webc/config"
	s "webc/structs"
	u "webc/utils"

	gs "github.com/termie/go-shutil"
)

func check(done chan bool, e error) {
	if e != nil {
		fmt.Printf("Worker ProjectCreator Error -> %s\n\n", e)
		done <- false
	}
}
func checkP(e error) {
	if e != nil {
		panic(fmt.Sprintf("Worker ProjectCreator Error -> %s\n\n", e))
	}
}

// CreateProject creates a project folder architecture
// based on a given Project and a destination
func CreateProject(done chan bool, p s.Project, dst string) {
	// creating the file for the porject configuration received in JSON
	json, err := json.Marshal(p)
	check(done, err)
	file, err := os.Create(filepath.Join(p.Directory, "webc.project.json"))
	check(done, err)
	// deferring closing of the file for after every use
	defer file.Close()
	// creating data as byte array
	data := []byte(json)
	file.Write(data)

	err = os.MkdirAll(p.ComponentsDirectory, os.ModeDir)
	check(done, err)
	err = os.MkdirAll(p.TemplateDirectory, os.ModeDir)
	check(done, err)
	err = os.MkdirAll(p.BuildDirectory, os.ModeDir)
	check(done, err)
	err = os.MkdirAll(p.AssetDirectory, os.ModeDir)
	check(done, err)

	fmt.Println(filepath.Join(ProcessFolder, config.TemplateDirectory, "index.html"))

	indexFile, err := os.Open(filepath.Join(ProcessFolder, config.TemplateDirectory, "index.html"))
	u.ReplaceMacrosInFileAndCopy(
		indexFile,
		filepath.Join(ProcessFolder, config.TemplateDirectory),
		filepath.Join(p.Directory),
		*s.CreateComponent("index", "WebComponent"),
		p,
		false,
	)

	gs.CopyTree(
		filepath.Join(ProcessFolder, config.TemplateDirectory, "lib"),
		filepath.Join(p.AssetDirectory, "lib"),
		nil,
	)

	done <- true
}

func ReadProject(done chan s.Project, projectName string) {
	// create an instance to receive the json decoded data
	project := s.Project{}
	// open the project file
	file, err := os.Open(filepath.Join(projectName, "webc.project.json"))
	defer file.Close()
	checkP(err)

	// create a byte array to receive the buffer
	fileStat, _ := file.Stat()
	fileData := make([]byte, fileStat.Size())
	// read the project file
	_, err = file.Read(fileData)
	checkP(err)

	// decode the json
	err = json.Unmarshal(fileData, &project)
	checkP(err)

	done <- project
}

package commands

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/urfave/cli"
)

var port string
var staticDirectory = "/"

// DevServer creates a server on the given port
// serving . by default
var DevServer = cli.Command{
	Name:    "serve",
	Aliases: []string{"s"},
	Usage:   "serve the actual project",
	Action: func(c *cli.Context) error {
		projectDirectory, _ := os.Getwd()
		argDirectory := c.Args().Get(0)
		if argDirectory == "" {
			argDirectory = staticDirectory
		}
		directory := filepath.Join(projectDirectory, argDirectory)
		fmt.Println("server listening directory " + directory + " on port " + port)
		log.Fatal(http.ListenAndServe(":"+port, http.FileServer(http.Dir(directory))))
		return nil
	},
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:        "port, p",
			Value:       "8080",
			Usage:       "set the port of the server",
			Destination: &port,
		},
	},
}

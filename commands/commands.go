package commands

import (
	"github.com/urfave/cli"
)

// Commands represents the Commands structure
// used with the cli app
type Commands struct {
	App      *cli.App
	Commands []cli.Command
}

// Create the commands for the cli app
func Create(app *cli.App) Commands {
	commands := []cli.Command{
		Init,
		CreateComponent,
		DevServer,
	}
	Commander := Commands{app, commands}
	return Commander
}

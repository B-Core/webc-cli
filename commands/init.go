package commands

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	s "webc/structs"
	w "webc/workers"
	u "webc/utils"
	"github.com/Songmu/prompter"
	"github.com/urfave/cli"
)

// Init create a job that will initiate the project
// create the webc.project.json and file structure
var Init = cli.Command{
	Name:    "init",
	Aliases: []string{"i"},
	Usage:   "initialize a project directory",
	Action: func(c *cli.Context) error {
		destination := c.Args().Get(1)
		projectName := c.Args().First()

		if destination == "" {
			destination, _ = os.Getwd()
		}
		// getting default name for the project if none is given
		if projectName == "" {
			path := strings.Split(destination, w.FileSeparator)
			projectName = path[len(path)-1]
		}
		
		if u.Exists(filepath.Join(destination, "webc.project.json")) {
			log.Fatal("Project already exists")
		}

		jsonWorker := make(chan bool, 0)

		projectName = prompter.Prompt("What's the name of your project? ", projectName)
		componentPath := filepath.Join(destination, prompter.Prompt("Components directory path: ", "components"))
		templatePath := filepath.Join(destination, prompter.Prompt("Templates directory path: ", "templates"))
		assetDirectory := filepath.Join(destination, prompter.Prompt("Assets directory path: ", "assets"))
		buildDirectory := filepath.Join(destination, prompter.Prompt("Build directory path: ", "builds"))

		project := s.Project{
			Name:                projectName,
			Directory:           destination,
			TemplateDirectory:   templatePath,
			AssetDirectory:      assetDirectory,
			BuildDirectory:      buildDirectory,
			ComponentsDirectory: componentPath,
		}

		go w.CreateProject(jsonWorker, project, destination)

		<-jsonWorker
		close(jsonWorker)

		return nil
	},
}

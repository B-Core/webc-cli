package commands

import (
	"fmt"
	"path/filepath"
	"log"
	"io/ioutil"
	"os"
	"strings"
	s "webc/structs"
	w "webc/workers"
	u "webc/utils"
	config "webc/config"

	"github.com/Songmu/prompter"
	"github.com/urfave/cli"
)

// CreateComponent create a job that will create a component
// on the file system based on a template
var CreateComponent = cli.Command{
	Name:    "create-component",
	Aliases: []string{"cp"},
	Usage:   "create a component",
	Action: func(c *cli.Context) error {
		componentWorker := make(chan s.Component)

		projectChan := make(chan s.Project)
		dir, _ := os.Getwd()
		go w.ReadProject(projectChan, dir)

		project := <-projectChan

		componentName := prompter.Prompt("Component Name: ", "")
		if strings.Trim(componentName, "") == "" {
			log.Fatal("you must enter a name")
		}

		if u.Exists(filepath.Join(project.ComponentsDirectory, componentName)) {
			log.Fatal(fmt.Sprintf("Component %s already exists", componentName))
		}

		// search for general templates in the webc directory
		// and in the personnal templates directory of the user
		componentTypes := []string{}
		srcTemplateDirectory := filepath.Join(w.ProcessFolder, config.TemplateDirectory)
		dstTemplateDirectory := project.TemplateDirectory
		templates, err := ioutil.ReadDir(srcTemplateDirectory)
		if err != nil {
			log.Fatal(err)
		}
		tpl, err := ioutil.ReadDir(dstTemplateDirectory)
		if err != nil { log.Fatal(err) }
		for _, t := range tpl { templates = append(templates, t) }

		for _, template := range templates {
			if template.IsDir() && template.Name() != "lib" {
				componentTypes = append(componentTypes, template.Name())
			}
		}

		componentType := prompter.Choose("which type: ", componentTypes, componentTypes[0])
		go w.CreateComponent(componentWorker, componentName, componentType, project)

		<-componentWorker
		close(componentWorker)
		close(projectChan)

		return nil
	},
}

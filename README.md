all commands are executed inside the project directory.

build command:
```bash
go build .
```

installation command:
```bash
go install .
```

test command :
```bash
bash test.sh
```
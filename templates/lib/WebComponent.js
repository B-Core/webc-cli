if (!String.prototype.toCamelCase) {
  String.prototype.toCamelCase = function(cap1st) {
    return ((cap1st ? "-" : "") + this).replace(/-+([^-])/g, function(a, b) {
      return b.toUpperCase();
    });
  };
}

const dep = [];

class WebComponent extends HTMLElement {
  constructor(name = "WebComponent", params) {
    super();
    this._name = name;
    this._shadowRoot = null;
    this._template = null;
    this._timeline = null;
    this._data = {
      data: params.data || {},
      style: params.style || {},
    };

    this._observablesElements = {};
    this.dependencies = (params.dependencies || []).concat(dep);
    this.animation = params.animation || {};

    this._createShadowRoot();
    this._updateRendering();
    this.applyEvents();

    this._loadDependencies(() => {
      if (this.created) this.created();
    });
  }

  attributeChangedCallback(name, oldValue, newValue) {
    const type = name.substr(0, name.indexOf("-"));
    const dataName = name.slice(name.indexOf("-") + 1);
    const attrOldValue = this.getAttribute(name);
    if (this.observables[type][dataName] !== newValue) {
      switch(type) {
        case 'data':
          this.observables[type][dataName] = newValue;
          break;
        case 'style':
          this.observables[type][dataName.toCamelCase()] = newValue;
          this.style.setProperty(`--${dataName}`, newValue);
          break;
      }
      if (attrOldValue !== newValue) this.setAttribute(name, newValue);
    }
    this._updateRendering();
  }

  applyEvents() {
    const eventMap = Array.from(this.attributes)
    .map(attribute => ({
      name: attribute.name,
      method: attribute.nodeValue,
    }))
    .filter(event => event.name[0] === '@')
    .map(event => ({
      name: event.name.slice(1),
      method: event.method,
    }));
    eventMap.forEach((event) => {
      this.addEventListener(event.name, this[event.method]);
    });
  }

  _createShadowRoot() {
    this._shadowRoot = this.attachShadow({mode: 'open'});
    
    const importDoc = document.currentScript.ownerDocument;
    this._template = importDoc.querySelector(`template[name="${this._name}"]`);

    const baseURI = document.currentScript.ownerDocument.baseURI;
    this._route = baseURI.slice(0, baseURI.lastIndexOf('/') + 1);
    const css = this._template.content.querySelector('link[rel="stylesheet"]');
    css.href = css.href.replace(`${document.baseURI}${window.encodeURIComponent('{{route}}')}`, this._route);

    this._shadowRoot.appendChild(this._template.content.cloneNode(true));
    this._createObservableMap();
    if (this.afterInit) this.afterInit();
  }

  _createObservableMap() {
    const elementsToObserve = Array.from(this._shadowRoot.querySelectorAll('[data-observe]'));
    elementsToObserve.forEach((element) => {
      const attribute = element.getAttribute('data-observe');
      if (!this._observablesElements[attribute]) this._observablesElements[attribute] = [];
      if (!this._observablesElements[attribute].find(el => element === el)) this._observablesElements[attribute].push(element);
    });
  }

  _updateObservables() {
    this.observablesKeys.forEach((attr) => {
      Object.keys(this.observables[attr]).forEach((type) => {
        if (this._observablesElements[type]) this._observablesElements[type].forEach(element => element.innerHTML = this.observables[attr][type]);
      });
    });
  }

  _updateRendering() {
    if (this.ownerDocument.defaultView) {
      this._updateObservables();
    }
  }

  _loadDependencies(callback) {
    $script(this.dependencies, callback);
  }

  set(type, dataName, value) {
    this.attributeChangedCallback(`${type}-${dataName}`, this.observables[type][dataName], value);
  }

  transformAnimationParams(params) {
    const p = {
      ...params,
    }
    return {
      ...p,
      targets: this,
      update: (anim) => {
        Object.keys(this._data.style).forEach((key) => {
          this.set('style', key, this.style[key]);
        });
      }
    }
  }

  addKeyframe(params) {
    const p = params instanceof Array ? this.transformAnimationParams(params) : [this.transformAnimationParams(params)];
    return this.timeline.add(p);
  }

  get timeline() {
    return this._timeline || anime.timeline({
      direction: this.animation.direction || undefined,
      loop: this.animation.loop || undefined,
      autoplay: true || this.animation.autoplay,
    });
  }

  get observablesAttributes() {
    return Object.keys(this.observables.data).map(key => `data-${key}`)
    .concat(Object.keys(this.observables.style).map(key => `style-${key}`))
  }

  get observablesKeys() {
    return Object.keys(this.observables);
  }

  get observables() {
    return this._data;
  }
}
class {{NAME}} extends WebComponent {
  constructor() {
    super("{{NAME}}",
    {
      data: {},
      style: {},
      dependencies: [],
    });
  }

  static get observedAttributes() {
    return [
      /*
        should refence the name of the data/style attributes to observe
        on the parent HTMLElement <your-component></your-component>
        like data-title
        or style-background-color
      */
    ];
  }
}

window.customElements.define("{{C_NAME}}", {{NAME}});
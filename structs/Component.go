package structs

import "github.com/satori/go.uuid"

type Component struct {
	id     uuid.UUID   `json: id`
	Name   string      `json: name`
	Type   string      `json: type`
	Childs []Component `json: childs`
}

// CreateComponent create a component with a valid uuid
func CreateComponent(name string, Type string) *Component {
	return &Component{
		id:   uuid.NewV4(),
		Name: name,
		Type: Type,
	}
}

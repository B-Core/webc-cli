package structs

type Project struct {
	Name                string `json: name`
	Directory           string `json: directory`
	TemplateDirectory   string `json: templateDirectory`
	AssetDirectory      string `json: assetDirectory`
	BuildDirectory      string `json: buildDirectory`
	ComponentsDirectory string `json: componentDirectory`
}
